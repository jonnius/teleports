# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-06 21:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#. TRANSLATORS: Application name.
#: ../push/pushhelper.cpp:114 ../app/qml/pages/ChatListPage.qml:21
#: ../app/qml/pages/AboutPage.qml:65 teleports.desktop.in.h:1
msgid "TELEports"
msgstr ""

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr ""

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr ""

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr ""

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr ""

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr ""

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr ""

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr ""

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr ""

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr ""

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr ""

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr ""

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr ""

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr ""

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr ""

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr ""

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr ""

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr ""

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr ""

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr ""

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr ""

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr ""

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr ""

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr ""

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../push/pushhelper.cpp:238
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
msgid "%1 removed %2"
msgstr ""

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr ""

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr ""

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr ""

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr ""

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr ""

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr ""

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr ""

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr ""

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr ""

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr ""

#: ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:301
msgid "Sticker"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:305
msgid "Phone call"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:309
msgid "sent an audio message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:313
msgid "sent a photo"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:322
msgid "Location"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:326
msgid "sent a video"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:330
msgid "sent a video note"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:334
msgid "sent a voice note"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:338
msgid "GIF"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:344
#: ../libs/qtdlib/messages/qtdmessage.cpp:348
msgid "joined the group"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:346
msgid "added one or more members"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:352
msgid "changed the chat photo"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:356
msgid "changed the chat title"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:360
msgid "joined by invite link"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:366
msgid "left the group"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:368
msgid "removed a member"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:373
msgid "deleted the chat photo"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:378
msgid "upgraded to supergroup"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:382
msgid "message TTL has been changed"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:387
msgid "created this group"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:396
msgid "has joined Telegram!"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:400
#: ../app/qml/delegates/MessageUnsupported.qml:4
msgid "Unsupported message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:404
msgid "Unimplemented:"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:409
msgid "Me"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessagecontent.cpp:8
msgid "Message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:227
msgid "Unread Messages"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:78
#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:31
msgid "Online"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr ""

#: ../libs/qtdlib/chat/qtdchat.cpp:89 ../app/qml/pages/ChatListPage.qml:53
msgid "Saved Messages"
msgstr ""

#: ../app/qml/components/UserProfile.qml:77
#: ../app/qml/components/UserProfile.qml:101
msgid "not available"
msgstr ""

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:39
msgid "Choose a country"
msgstr ""

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr ""

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr ""

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:31
msgid "Cancel"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:21
msgid "Are you sure you want to clear the history?"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:22
#: ../app/qml/pages/ChatListPage.qml:129
msgid "Clear history"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:31
msgid "Are you sure you want to leave this chat?"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:32
msgid "Leave"
msgstr ""

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:60
msgid "Invalid phone number!"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:67
msgid "Invalid code!"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:74
msgid "Invalid password!"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:94
msgid "Auth code not expected right now"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:100
msgid "Oops! Internal error."
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:106
msgid "Incorrect auth code length."
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr ""

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "Error"
msgstr ""

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "No valid location received after 180 seconds!"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:18
msgid "Enter Code"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:44
msgid "First Name"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:52
msgid "Last Name"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:59
msgid "Code"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:74
msgid "We've send a code via telegram to your device. Please enter it here."
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:20
msgid "Select destination or cancel..."
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:37 ../app/qml/pages/ChatListPage.qml:63
#: ../app/qml/pages/SettingsPage.qml:23
msgid "Settings"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:58 ../app/qml/pages/UserListPage.qml:18
msgid "Contacts"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:69
msgid "Night mode"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:82 ../app/qml/pages/AboutPage.qml:13
msgid "About"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:124
msgid "Leave chat"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:140 ../app/qml/pages/UserListPage.qml:122
msgid "Info"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:350
msgid "Do you want to forward the selected messages to %1?"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:352
#: ../app/qml/delegates/MessageBubbleItem.qml:86
msgid "Forward"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:366 ../app/qml/pages/ChatListPage.qml:390
msgid "Enter optional message..."
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:374
msgid "Do you want to send the imported files to %1?"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:376
#: ../app/qml/pages/MessageListPage.qml:525
msgid "Send"
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:57
#: ../app/qml/pages/WaitPhoneNumberPage.qml:67
msgid "Phone number"
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:91
msgid "Please confirm your country code and enter your phone number."
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:95
#: ../app/qml/pages/WaitPasswordPage.qml:65
msgid "Next..."
msgstr ""

#: ../app/qml/pages/UserListPage.qml:29
#: ../app/qml/pages/GroupDetailsPage.qml:32 ../app/qml/pages/AboutPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:47
#: ../app/qml/pages/SettingsPage.qml:34 ../app/qml/pages/UserProfilePage.qml:31
msgid "Back"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:47
msgid "The contact will be added. First and last name are optional"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:49
msgid "Add"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:62 ../app/qml/pages/UserProfilePage.qml:66
msgid "Phone no"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:73 ../app/qml/pages/UserProfilePage.qml:78
msgid "First name"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:84 ../app/qml/pages/UserProfilePage.qml:90
msgid "Last name"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:112 ../app/qml/pages/UserListPage.qml:214
#: ../app/qml/pages/SettingsPage.qml:120
#: ../app/qml/delegates/MessageBubbleItem.qml:33
#: ../app/qml/delegates/MessageBubbleItem.qml:47
msgid "Delete"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:127
msgid "Secret Chat"
msgstr ""

#: ../app/qml/pages/UserListPage.qml:212
msgid "The contact will be deleted. Are you sure?"
msgstr ""

#: ../app/qml/pages/GroupDetailsPage.qml:21
msgid "Group Details"
msgstr ""

#: ../app/qml/pages/GroupDetailsPage.qml:99
msgid "Members: %1"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:71
msgid "Version %1"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:73
msgid " (git# %1)"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:98
msgid "Get the source"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:99
msgid "Report issues"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:100
msgid "Help translate"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Offline"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Connecting"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting To Proxy"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:30
msgid "Updating"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:36
msgid "Connectivity"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:75
msgid "Telegram connectivity status:"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:82
msgid "Ubuntu Touch connectivity status:"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:89
msgid "Ubuntu Touch bandwith limited"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:89
msgid "Ubuntu Touch bandwith not limited"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:65 ../app/qml/pages/SettingsPage.qml:110
msgid "Logout"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:78
msgid "Delete account"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:91
msgid "Connectivity status"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:108
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:118
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"send using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr ""

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr ""

#: ../app/qml/pages/PreviewPage.qml:49 ../app/qml/pages/UserProfilePage.qml:52
msgid "Save"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:29
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] ""
msgstr[1] ""

#: ../app/qml/pages/MessageListPage.qml:31
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] ""
msgstr[1] ""

#: ../app/qml/pages/MessageListPage.qml:127
msgid "Telegram"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:296
#: ../app/qml/pages/UserProfilePage.qml:40
#: ../app/qml/delegates/MessageBubbleItem.qml:66
msgid "Edit"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:296
#: ../app/qml/delegates/MessageBubbleItem.qml:74
msgid "Reply"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:319
msgid "You are not allowed to post in this channel"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:323
msgid "Waiting for other party to accept the secret chat..."
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:325
msgid "Secret chat has been closed"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:413
msgid "Type a message..."
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:523
msgid "Do you want to share your location with %1?"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:536
msgid "Requesting location from OS..."
msgstr ""

#: ../app/qml/pages/UserProfilePage.qml:20
msgid "Profile"
msgstr ""

#: ../app/qml/pages/UserProfilePage.qml:50
msgid "Edit user data and press Save"
msgstr ""

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr ""

#: ../app/qml/pages/WaitPasswordPage.qml:42
msgid "Password"
msgstr ""

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:11
msgid "%1 joined the group"
msgstr ""

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:12
msgid "%1 added %2"
msgstr ""

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:35
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] ""
msgstr[1] ""

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr ""

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:44
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:45
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:56
msgid "Copy"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:80
msgid "Sticker Pack info"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:239
msgid "Edited"
msgstr ""

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr ""

#: ../app/qml/delegates/MessageContentBase.qml:34
msgid "Forwarded from %1"
msgstr ""

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr ""

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr ""

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:51
msgid "Call Declined"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:53
msgid "Call Disconnected"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:56
msgid "Call Ended"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:58
msgid "Cancelled Call"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:58
msgid "Missed Call"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:71
msgid "Duration: %1:%2"
msgstr ""

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called << %1 >> created"
msgstr ""

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called << %2 >>"
msgstr ""

#: ../app/qml/delegates/MessageDateItem.qml:4
msgid "dd MMMM"
msgstr ""

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr ""

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr ""

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-06 21:54+0000\n"
"PO-Revision-Date: 2019-11-09 23:36+0000\n"
"Last-Translator: Ivo Xavier <ivofernandes12@gmail.com>\n"
"Language-Team: Portuguese <https://translate.ubports.com/projects/ubports/"
"teleports/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.8\n"

#. TRANSLATORS: Application name.
#: ../push/pushhelper.cpp:114 ../app/qml/pages/ChatListPage.qml:21
#: ../app/qml/pages/AboutPage.qml:65 teleports.desktop.in.h:1
msgid "TELEports"
msgstr "TELEports"

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr "enviou-lhe uma mensagem"

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr "enviou-lhe uma foto"

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr "enviou-lhe um autocolante"

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr "enviou-lhe um vídeo"

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr "enviou-lhe um documento"

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr "enviou uma mensagem de áudio"

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr "enviou-lhe uma mensagem de voz"

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr "partilhou um contacto consigo"

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr "enviou-lhe um mapa"

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr "%1: %2"

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr "%1 enviou-lhe uma mensagem para o grupo"

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr "%1 enviou uma foto para o grupo"

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr "%1 enviou um autocolante para o grupo"

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr "%1 enviou um vídeo para o grupo"

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr "%1 enviou um documento para o grupo"

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr "%1 enviou um mensagem de voz para o grupo"

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr "%1 enviou um GIF para o grupo"

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr "%1 enviou um contacto para o grupo"

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr "%1 enviou um mapa para o grupo"

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr "%1 convidou-o para o grupo"

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr "%1 alterou o nome do grupo"

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr "%1 alterou a foto do grupo"

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr "%1 convidou %2"

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../push/pushhelper.cpp:238
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
msgid "%1 removed %2"
msgstr "%1 removeu %2"

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr "%1 removeu-o do grupo"

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr "%1 saiu do grupo"

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr "%1 voltou ao grupo"

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr "@ %1"

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr "%1 entrou"

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr "%1 juntou-se ao Telegram!"

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr "Novo início de sessão através de um dispositivo desconhecido"

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr "%1 @ %2"

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr "atualizou a foto de perfil"

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr "Você tem uma mensagem nova"

#: ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr "%1 fixou uma mensagem"

#: ../libs/qtdlib/messages/qtdmessage.cpp:301
msgid "Sticker"
msgstr "Autocolante"

#: ../libs/qtdlib/messages/qtdmessage.cpp:305
msgid "Phone call"
msgstr "Chamada telefónica"

#: ../libs/qtdlib/messages/qtdmessage.cpp:309
msgid "sent an audio message"
msgstr "enviou-lhe uma mensagem de áudio"

#: ../libs/qtdlib/messages/qtdmessage.cpp:313
msgid "sent a photo"
msgstr "enviou uma foto"

#: ../libs/qtdlib/messages/qtdmessage.cpp:322
msgid "Location"
msgstr "Localização"

#: ../libs/qtdlib/messages/qtdmessage.cpp:326
msgid "sent a video"
msgstr "enviou um vídeo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:330
msgid "sent a video note"
msgstr "enviou um nota em vídeo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:334
msgid "sent a voice note"
msgstr "enviou uma nota de voz"

#: ../libs/qtdlib/messages/qtdmessage.cpp:338
msgid "GIF"
msgstr "GIF"

#: ../libs/qtdlib/messages/qtdmessage.cpp:344
#: ../libs/qtdlib/messages/qtdmessage.cpp:348
msgid "joined the group"
msgstr "juntou-se ao grupo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:346
msgid "added one or more members"
msgstr "adicionou um ou mais membros"

#: ../libs/qtdlib/messages/qtdmessage.cpp:352
msgid "changed the chat photo"
msgstr "alterou a foto do grupo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:356
msgid "changed the chat title"
msgstr "alterou o título da conversa"

#: ../libs/qtdlib/messages/qtdmessage.cpp:360
msgid "joined by invite link"
msgstr "juntou-se através de convite"

#: ../libs/qtdlib/messages/qtdmessage.cpp:366
msgid "left the group"
msgstr "saiu do grupo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:368
msgid "removed a member"
msgstr "removeu um membro"

#: ../libs/qtdlib/messages/qtdmessage.cpp:373
msgid "deleted the chat photo"
msgstr "eliminou a foto da conversa"

#: ../libs/qtdlib/messages/qtdmessage.cpp:378
msgid "upgraded to supergroup"
msgstr "atualizou para supergrupo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:382
msgid "message TTL has been changed"
msgstr "mensagem TTL foi alterada"

#: ../libs/qtdlib/messages/qtdmessage.cpp:387
msgid "created this group"
msgstr "criou este grupo"

#: ../libs/qtdlib/messages/qtdmessage.cpp:396
msgid "has joined Telegram!"
msgstr "juntou-se ao Telegram!"

#: ../libs/qtdlib/messages/qtdmessage.cpp:400
#: ../app/qml/delegates/MessageUnsupported.qml:4
msgid "Unsupported message"
msgstr "Mensagem não suportada"

#: ../libs/qtdlib/messages/qtdmessage.cpp:404
msgid "Unimplemented:"
msgstr "Não implementado:"

#: ../libs/qtdlib/messages/qtdmessage.cpp:409
msgid "Me"
msgstr "Eu"

#: ../libs/qtdlib/messages/qtdmessagecontent.cpp:8
msgid "Message"
msgstr "Mensagem"

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:227
msgid "Unread Messages"
msgstr "Mensagens não lidas"

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr "Última vez visto há um mês"

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr "Última vez visto há uma semana"

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr "Visto pela última vez "

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr "dd.MM.yy hh:mm"

#: ../libs/qtdlib/user/qtduserstatus.cpp:78
#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:31
msgid "Online"
msgstr "Ligado"

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr "Visto recentemente"

#: ../libs/qtdlib/chat/qtdchat.cpp:89 ../app/qml/pages/ChatListPage.qml:53
msgid "Saved Messages"
msgstr "Mensagens guardadas"

#: ../app/qml/components/UserProfile.qml:77
#: ../app/qml/components/UserProfile.qml:101
msgid "not available"
msgstr "não disponível"

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:39
msgid "Choose a country"
msgstr "Escolha um país"

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr "Procurar o nome do país..."

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr "Ok"

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:31
msgid "Cancel"
msgstr "Cancelar"

#: ../app/qml/middleware/ChatMiddleware.qml:21
msgid "Are you sure you want to clear the history?"
msgstr "Tem a certeza que deseja limpar o histórico?"

#: ../app/qml/middleware/ChatMiddleware.qml:22
#: ../app/qml/pages/ChatListPage.qml:129
msgid "Clear history"
msgstr "Limpar histórico"

#: ../app/qml/middleware/ChatMiddleware.qml:31
msgid "Are you sure you want to leave this chat?"
msgstr "Tem a certeza que deseja sair desta conversa?"

#: ../app/qml/middleware/ChatMiddleware.qml:32
msgid "Leave"
msgstr "Sair"

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr "Fechar"

#: ../app/qml/stores/AuthStateStore.qml:60
msgid "Invalid phone number!"
msgstr "Número de telefone inválido!"

#: ../app/qml/stores/AuthStateStore.qml:67
msgid "Invalid code!"
msgstr "Código inválido!"

#: ../app/qml/stores/AuthStateStore.qml:74
msgid "Invalid password!"
msgstr "Palavra-passe inválida!"

#: ../app/qml/stores/AuthStateStore.qml:94
msgid "Auth code not expected right now"
msgstr "Código de autenticação não esperado agora"

#: ../app/qml/stores/AuthStateStore.qml:100
msgid "Oops! Internal error."
msgstr "Oops! Erro interno."

#: ../app/qml/stores/AuthStateStore.qml:106
msgid "Incorrect auth code length."
msgstr "Comprimento do código de autenticação incorreto."

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr "Falhou o registo no servidor Push"

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr "Sem Ubuntu One"

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr "Por favor, conectar-se ao Ubuntu One para receber notificações."

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "Error"
msgstr "Erro"

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "No valid location received after 180 seconds!"
msgstr "Sem localização válida recebida após 180 segundos!"

#: ../app/qml/pages/WaitCodePage.qml:18
msgid "Enter Code"
msgstr "Introduza o código"

#: ../app/qml/pages/WaitCodePage.qml:44
msgid "First Name"
msgstr "Primeiro nome"

#: ../app/qml/pages/WaitCodePage.qml:52
msgid "Last Name"
msgstr "Apelido"

#: ../app/qml/pages/WaitCodePage.qml:59
msgid "Code"
msgstr "Código"

#: ../app/qml/pages/WaitCodePage.qml:74
msgid "We've send a code via telegram to your device. Please enter it here."
msgstr ""
"Enviamos-lhes um código via telegram para o seu dispositivo. Por favor "
"introduza-o aqui."

#: ../app/qml/pages/ChatListPage.qml:20
msgid "Select destination or cancel..."
msgstr "Escolha o destino ou cancele..."

#: ../app/qml/pages/ChatListPage.qml:37 ../app/qml/pages/ChatListPage.qml:63
#: ../app/qml/pages/SettingsPage.qml:23
msgid "Settings"
msgstr "Definições"

#: ../app/qml/pages/ChatListPage.qml:58 ../app/qml/pages/UserListPage.qml:18
msgid "Contacts"
msgstr "Contactos"

#: ../app/qml/pages/ChatListPage.qml:69
msgid "Night mode"
msgstr "Modo noturno"

#: ../app/qml/pages/ChatListPage.qml:82 ../app/qml/pages/AboutPage.qml:13
msgid "About"
msgstr "Sobre"

#: ../app/qml/pages/ChatListPage.qml:124
msgid "Leave chat"
msgstr "Sair da conversa"

#: ../app/qml/pages/ChatListPage.qml:140 ../app/qml/pages/UserListPage.qml:122
msgid "Info"
msgstr "Info"

#: ../app/qml/pages/ChatListPage.qml:350
msgid "Do you want to forward the selected messages to %1?"
msgstr "Deseja reencaminhar as mensagens selecionadas para %1?"

#: ../app/qml/pages/ChatListPage.qml:352
#: ../app/qml/delegates/MessageBubbleItem.qml:86
msgid "Forward"
msgstr "Reencaminhar"

#: ../app/qml/pages/ChatListPage.qml:366 ../app/qml/pages/ChatListPage.qml:390
msgid "Enter optional message..."
msgstr "Escreva uma mensagem opcional..."

#: ../app/qml/pages/ChatListPage.qml:374
msgid "Do you want to send the imported files to %1?"
msgstr "Deseja enviar os ficheiros importados para %1?"

#: ../app/qml/pages/ChatListPage.qml:376
#: ../app/qml/pages/MessageListPage.qml:525
msgid "Send"
msgstr "Enviar"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr "Introduza o número de telefone"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:57
#: ../app/qml/pages/WaitPhoneNumberPage.qml:67
msgid "Phone number"
msgstr "Número de telefone"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:91
msgid "Please confirm your country code and enter your phone number."
msgstr ""
"Por favor, confirme o código do seus país e introduza o seu número de "
"telefone."

#: ../app/qml/pages/WaitPhoneNumberPage.qml:95
#: ../app/qml/pages/WaitPasswordPage.qml:65
msgid "Next..."
msgstr "Próximo..."

#: ../app/qml/pages/UserListPage.qml:29
#: ../app/qml/pages/GroupDetailsPage.qml:32 ../app/qml/pages/AboutPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:47
#: ../app/qml/pages/SettingsPage.qml:34 ../app/qml/pages/UserProfilePage.qml:31
msgid "Back"
msgstr "Voltar"

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr "Adicionar contacto"

#: ../app/qml/pages/UserListPage.qml:47
msgid "The contact will be added. First and last name are optional"
msgstr "O contacto será adicionado. Primeiro e último nome são opcionais"

#: ../app/qml/pages/UserListPage.qml:49
msgid "Add"
msgstr "Adicionar"

#: ../app/qml/pages/UserListPage.qml:62 ../app/qml/pages/UserProfilePage.qml:66
msgid "Phone no"
msgstr "Número telef"

#: ../app/qml/pages/UserListPage.qml:73 ../app/qml/pages/UserProfilePage.qml:78
msgid "First name"
msgstr "Primeiro nome"

#: ../app/qml/pages/UserListPage.qml:84 ../app/qml/pages/UserProfilePage.qml:90
msgid "Last name"
msgstr "Apelido"

#: ../app/qml/pages/UserListPage.qml:112 ../app/qml/pages/UserListPage.qml:214
#: ../app/qml/pages/SettingsPage.qml:120
#: ../app/qml/delegates/MessageBubbleItem.qml:33
#: ../app/qml/delegates/MessageBubbleItem.qml:47
msgid "Delete"
msgstr "Eliminar"

#: ../app/qml/pages/UserListPage.qml:127
msgid "Secret Chat"
msgstr "Conversa secreta"

#: ../app/qml/pages/UserListPage.qml:212
msgid "The contact will be deleted. Are you sure?"
msgstr "O contacto será eliminado. Tem a certeza?"

#: ../app/qml/pages/GroupDetailsPage.qml:21
msgid "Group Details"
msgstr "Detalhes do grupo"

#: ../app/qml/pages/GroupDetailsPage.qml:99
msgid "Members: %1"
msgstr "Membros: %1"

#: ../app/qml/pages/AboutPage.qml:71
msgid "Version %1"
msgstr "Versão %1"

#: ../app/qml/pages/AboutPage.qml:73
msgid " (git# %1)"
msgstr " (git# %1)"

#: ../app/qml/pages/AboutPage.qml:98
msgid "Get the source"
msgstr "Obter o código-fonte"

#: ../app/qml/pages/AboutPage.qml:99
msgid "Report issues"
msgstr "Reportar falhas"

#: ../app/qml/pages/AboutPage.qml:100
msgid "Help translate"
msgstr "Ajudar a traduzir"

#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Offline"
msgstr "Desligado"

#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Connecting"
msgstr "A ligar"

#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting To Proxy"
msgstr "A ligar ao proxy"

#: ../app/qml/pages/ConnectivityPage.qml:30
msgid "Updating"
msgstr "A atualizar"

#: ../app/qml/pages/ConnectivityPage.qml:36
msgid "Connectivity"
msgstr "Conetividade"

#: ../app/qml/pages/ConnectivityPage.qml:75
msgid "Telegram connectivity status:"
msgstr "Estado da conexão Telegram:"

#: ../app/qml/pages/ConnectivityPage.qml:82
msgid "Ubuntu Touch connectivity status:"
msgstr "Estado da conetividade Ubuntu Touch:"

#: ../app/qml/pages/ConnectivityPage.qml:89
msgid "Ubuntu Touch bandwith limited"
msgstr "Banda larga limitada Ubuntu Touch"

#: ../app/qml/pages/ConnectivityPage.qml:89
msgid "Ubuntu Touch bandwith not limited"
msgstr "Banda larga não limitada Ubuntu Touch"

#: ../app/qml/pages/SettingsPage.qml:65 ../app/qml/pages/SettingsPage.qml:110
msgid "Logout"
msgstr "Terminar sessão"

#: ../app/qml/pages/SettingsPage.qml:78
msgid "Delete account"
msgstr "Eliminar conta"

#: ../app/qml/pages/SettingsPage.qml:91
msgid "Connectivity status"
msgstr "Estado de conetividade"

#: ../app/qml/pages/SettingsPage.qml:108
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""
"Aviso: Terminar sessão apenas vai eliminar os dados locais deste "
"dispositivos, incluindo as conversas secretas. Ainda tem a certeza que quer "
"terminar a sessão?"

#: ../app/qml/pages/SettingsPage.qml:118
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"send using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""
"Aviso: Eliminar a conta vai eliminar todos os dados recebidos ou enviados "
"desde que usa o Telegram, expecto aqueles que guardou fora do Telegram. "
"Deseja mesmo eliminar a conta?"

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr "Seletor de conteúdo"

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr "Ficheiro: "

#: ../app/qml/pages/PreviewPage.qml:49 ../app/qml/pages/UserProfilePage.qml:52
msgid "Save"
msgstr "Guardar"

#: ../app/qml/pages/MessageListPage.qml:29
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] "%1 membro"
msgstr[1] "%1 membros"

#: ../app/qml/pages/MessageListPage.qml:31
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] ", %1 ligado"
msgstr[1] ", %1 ligados"

#: ../app/qml/pages/MessageListPage.qml:127
msgid "Telegram"
msgstr "Telegram"

#: ../app/qml/pages/MessageListPage.qml:296
#: ../app/qml/pages/UserProfilePage.qml:40
#: ../app/qml/delegates/MessageBubbleItem.qml:66
msgid "Edit"
msgstr "Editar"

#: ../app/qml/pages/MessageListPage.qml:296
#: ../app/qml/delegates/MessageBubbleItem.qml:74
msgid "Reply"
msgstr "Responder"

#: ../app/qml/pages/MessageListPage.qml:319
msgid "You are not allowed to post in this channel"
msgstr "Não tem permissão para conversar neste canal"

#: ../app/qml/pages/MessageListPage.qml:323
msgid "Waiting for other party to accept the secret chat..."
msgstr "À espera pela outra parte para aceitar o chat secreto..."

#: ../app/qml/pages/MessageListPage.qml:325
msgid "Secret chat has been closed"
msgstr "Chat secreto foi fechado"

#: ../app/qml/pages/MessageListPage.qml:413
msgid "Type a message..."
msgstr "Escrever mensagem..."

#: ../app/qml/pages/MessageListPage.qml:523
msgid "Do you want to share your location with %1?"
msgstr "Deseja partilhar a localização com %1?"

#: ../app/qml/pages/MessageListPage.qml:536
msgid "Requesting location from OS..."
msgstr "A pedir localização ao SO..."

#: ../app/qml/pages/UserProfilePage.qml:20
msgid "Profile"
msgstr "Perfil"

#: ../app/qml/pages/UserProfilePage.qml:50
msgid "Edit user data and press Save"
msgstr "Editar dados e selecionar Guardar"

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr "Introduza a palavra-chave"

#: ../app/qml/pages/WaitPasswordPage.qml:42
msgid "Password"
msgstr "Palavra-chave"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:11
msgid "%1 joined the group"
msgstr "%1 juntou-se ao grupo"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:12
msgid "%1 added %2"
msgstr "%1 adicionou %2"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:35
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] "%1 utilizador(es)"
msgstr[1] "%1 utilizadores"

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr "%1 saiu do grupo"

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr "Grupo atualizado para Supergrupo"

#: ../app/qml/delegates/MessageBubbleItem.qml:44
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""
"A mensagem irá ser eliminada para todos os utilizadores do chat. Deseja "
"mesmo eliminar?"

#: ../app/qml/delegates/MessageBubbleItem.qml:45
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr "A mensagem será eliminada apenas para si. Deseja mesmo eliminar?"

#: ../app/qml/delegates/MessageBubbleItem.qml:56
msgid "Copy"
msgstr "Copiar"

#: ../app/qml/delegates/MessageBubbleItem.qml:80
msgid "Sticker Pack info"
msgstr "Informação do pacote de autocolantes"

#: ../app/qml/delegates/MessageBubbleItem.qml:239
msgid "Edited"
msgstr "Editado"

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr "Uma captura de ecrã foi tirada"

#: ../app/qml/delegates/MessageContentBase.qml:34
msgid "Forwarded from %1"
msgstr "Encaminhado de %1"

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr "%1 juntou-se ao Telegram!"

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr ""
"Tipo de mensagem desconhecida, verifique o registo para mais detalhes..."

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr "Etiqueta em falta..."

#: ../app/qml/delegates/MessageContentCall.qml:51
msgid "Call Declined"
msgstr "Chamada rejeitada"

#: ../app/qml/delegates/MessageContentCall.qml:53
msgid "Call Disconnected"
msgstr "Chamada desligada"

#: ../app/qml/delegates/MessageContentCall.qml:56
msgid "Call Ended"
msgstr "Chamada terminada"

#: ../app/qml/delegates/MessageContentCall.qml:58
msgid "Cancelled Call"
msgstr "Chamada cancelada"

#: ../app/qml/delegates/MessageContentCall.qml:58
msgid "Missed Call"
msgstr "Chamada perdida"

#: ../app/qml/delegates/MessageContentCall.qml:71
msgid "Duration: %1:%2"
msgstr "Duração: %1:%2"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called << %1 >> created"
msgstr "Canal << %1 >> criado"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called << %2 >>"
msgstr "%1 criou um grupo chamado << %2 >>"

#: ../app/qml/delegates/MessageDateItem.qml:4
msgid "dd MMMM"
msgstr "dd MMMM"

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr "%1 juntou-se através de convite"

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr "Nota de voz"

#~ msgid "Image"
#~ msgstr "Imagem"

#~ msgid "File"
#~ msgstr "Ficheiro"

#~ msgid "Video"
#~ msgstr "Vídeo"

#~ msgid "Audio"
#~ msgstr "Áudio"

#~ msgid "sent an unknown message: %1"
#~ msgstr "enviou uma mensagem desconhecida: %1"

#~ msgid "Status:"
#~ msgstr "Estado:"

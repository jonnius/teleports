import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import Ubuntu.Content 1.1 as ContentHub
import QuickFlux 1.1
import QTelegram 1.0
import QTelegramStyles 1.0
import "../components"
import "../actions"
import "../delegates"
import "../stores"

Page {
    id: messageListPage
    property QTdChat currentChat: Telegram.chats.currentChat
    property int currentMessageIndex: currentChat.currentMessageIndex
    property var locationWaitDialog: null

    header: UITK.PageHeader {
        title: Telegram.chats && currentChat ? currentChat.title : ""
        subtitle: {
            if (Telegram.chats && currentChat) {
                if (currentChat.action != "")
                    return currentChat.action;
                if (currentChat.isGroup || currentChat.isChannel) {
                    var memberCountInfo = i18n.tr("%1 member", "%1 members", currentChat.memberCount).arg(currentChat.memberCount);
                    if (currentChat.onlineMemberCount > 0) {
                        memberCountInfo += i18n.tr(", %1 online", ", %1 online", currentChat.onlineMemberCount).arg(currentChat.onlineMemberCount);
                    }
                    return memberCountInfo;
                }
                return currentChat.chatType.user.status.string
            }
            return "";
        }
        leadingActionBar.actions: [
            UITK.Action {
                iconName: "back"
                text: "Back"
                onTriggered: {
                    AppActions.chat.closeCurrentChat()
                }
            }
        ]
        contents: Item {
            anchors.fill: parent

            GenericPhoto {
                id: avatar

                anchors {
                    verticalCenter: parent.verticalCenter
                    leftMargin: units.dp(4)
                }
                height: units.gu(5)
                width: height
                property QTdLocalFile localFile: Telegram.chats.currentChat.chatPhoto.small.local
                photoPath: {
                    if (localFile && localFile.canBeDownloaded && !localFile.isDownloadingCompleted) {
                        Telegram.chats.currentChat.chatPhoto.small.downloadFile();
                    }
                    return Telegram.chats.currentChat && localFile.path ? localFile.path : ""
                }
                initials: Telegram.chats.currentChat ? Telegram.chats.currentChat.initials : "N/A"
                avatarColor: Telegram.chats.currentChat.avatarColor(Telegram.chats.currentChat ? Telegram.chats.currentChat.rawId : 0)
                myself: Telegram.chats.currentChat ? Telegram.chats.currentChat.isMyself : false
            }

            Rectangle {
                id: connectingIndicator
                anchors.fill: avatar
                visible: false
                color: theme.palette.normal.background
                UITK.Icon {
                    name: "sync-updating" // telegram.busy? "sync-updating" : isConnecting? "sync-error" : "sync-paused"
                    anchors.fill: parent
                    color: Theme.palette.normal.backgroundSecondaryText
                }
                SequentialAnimation {
                    running: visible
                    loops: Animation.Infinite
                    PropertyAnimation {
                        target: connectingIndicator;property: "opacity";to: 1;duration: 500
                    }
                    PropertyAnimation {
                        duration: 300
                    }
                    PropertyAnimation {
                        target: connectingIndicator;property: "opacity";to: 0.0;duration: 800
                    }
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }

            UITK.Icon {
                id: secret_chat_icon
                visible: Telegram.chats.currentChat.isSecret
                name: "network-secure"
                anchors {
                    top: parent.top
                    left: avatar.right
                    topMargin: units.dp(5)
                    bottomMargin: units.dp(5)
                    bottom: titleText.bottom
                }
                width: height
            }
            Label {
                id: titleText
                anchors {
                    top: parent.top
                    left: secret_chat_icon.visible ? secret_chat_icon.right : avatar.right
                    leftMargin: units.gu(1)
                    right: parent.right
                }
                verticalAlignment: Text.AlignVCenter

                font.pixelSize: FontUtils.sizeToPixels("large")
                elide: Text.ElideRight
                wrapMode: Text.WrapAnywhere
                maximumLineCount: 1
                text: header.title.length === 0 ? i18n.tr("Telegram") : header.title

                state: header.subtitle.length > 0 ? "subtitle" : "default"
                states: [
                    State {
                        name: "default"
                        AnchorChanges {
                            target: titleText
                            anchors.verticalCenter: titleText.parent.verticalCenter
                        }
                        PropertyChanges {
                            target: titleText
                            height: titleText.implicitHeight
                            anchors.topMargin: units.gu(0.7)
                        }
                    },
                    State {
                        name: "subtitle"
                        PropertyChanges {
                            target: titleText
                            height: titleText.parent.height / 2
                            anchors.topMargin: units.gu(0.35)
                        }
                    }
                ]

                transitions: [
                    Transition {
                        AnchorAnimation {
                            duration: UITK.UbuntuAnimation.FastDuration
                        }
                    }
                ]
            }

            Label {
                id: subtitleText
                anchors {
                    left: avatar.right
                    leftMargin: units.gu(1)
                    bottom: parent.bottom
                    bottomMargin: units.gu(0.15)
                }
                verticalAlignment: Text.AlignVCenter
                height: parent.height / 2
                width: parent.width

                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                maximumLineCount: 1
                text: header.subtitle
                color: header.isOnline ? UITK.UbuntuColors.blue : theme.palette.normal.backgroundTertiaryText

                Connections {
                    target: header
                    onSubtitleChanged: {
                        subtitleText.opacity = 0;
                        subtitleText.text = "";
                        subtitleTextTimer.start();
                    }
                }

                Timer {
                    id: subtitleTextTimer
                    interval: UITK.UbuntuAnimation.FastDuration
                    onTriggered: {
                        subtitleText.text = header.subtitle;
                        subtitleText.opacity = 1;
                    }
                }

                Behavior on opacity {
                    NumberAnimation {
                        duration: UITK.UbuntuAnimation.FastDuration
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    mouse.accepted = true;
                    currentChat.isPrivate ? AppActions.user.showUserInfo(currentChat.chatType.user) : AppActions.chat.viewGroupInfo(currentChat)
                }
            }
        }
    }

    WaitingBar {
        id: waitingBar
        connectionState: Telegram.connections.connectionState
        z: 10
    }

    FlyingButton {
        id: scrollDownButton
        onClicked: {
            AppActions.chat.jumpToMessage(Telegram.chats.currentChat.lastMessage.id)
        }
        iconName: "toolkit_chevron-down_3gu"
        anchors.bottom: infoBox.enabled ? infoBox.top : msgListScrollView.bottom
        visibleState: msgList.visibleArea.yPosition < 1.0 - msgList.visibleArea.heightRatio - 0.01 && !attach_panel_object.visible
    }

    ScrollView {
        id: msgListScrollView
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            bottom: writableChatInfo.visible? writableChatInfo.top : input.top
        }

        ListView {
            id: msgList
            anchors {
                fill: parent
                bottomMargin: Suru.units.gu(1)
            }

            //highlightRangeMode: ListView.StrictlyEnforceRange
            highlightRangeMode: ListView.ApplyRange

            model: Telegram.chats.messageList
            verticalLayoutDirection: ListView.BottomToTop
            delegate: Component {
                Loader {
                    id: loader
                    width: parent.width
                    height: childrenRect.height
                    Component.onCompleted: setSource(delegateMap.findComponent(modelData.content.type), {
                        message: modelData,
                        chat: Telegram.chats.chat
                    })
                    asynchronous: false
                }
            }

            visibleArea.onYPositionChanged: {
                if (visibleArea.yPosition < 0.05 && model.count > 10) {
                    AppActions.chat.loadOlderMessages()
                }

                if (visibleArea.yPosition > 1 - visibleArea.heightRatio - 0.05 && model.count > 10) {
                    AppActions.chat.loadNewerMessages()
                }
            }

            onMovingChanged: {
                if (moving && highlightRangeMode != ListView.NoHighlightRange) {
                    // this would smooth scrolling maybe, but must not be set when jumping to messages
                    // highlightRangeMode = ListView.NoHighlightRange;
                }
            }
        }
    }

    InputInfoBox {
        id: infoBox
        // Convenience properties
        readonly property bool editingMessage: d.chatState === ChatState.EditingMessage
        readonly property bool replyingToMessage: d.chatState === ChatState.ReplyingToMessage

        anchors {
            left: parent.left
            right: parent.right
            bottom: writableChatInfo.visible? writableChatInfo.top : input.top
        }
        enabled: editingMessage || replyingToMessage
        title: editingMessage ? i18n.tr("Edit") : i18n.tr("Reply")
        message: d.messageOfInterest
        onCloseRequested: {
            if (editingMessage) {
                entry.text = "";
            }
            d.chatState = ChatState.Default
        }
    }

    UITK.Label {
        id: writableChatInfo
        anchors {
            left: parent.left
            right: parent.right
            bottom: showKeyboardLoader.top
        }
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        height: entry.height + Suru.units.gu(2)
        visible: !currentChat.isWritable
        text: {
            if (currentChat.isChannel) {
                return i18n.tr("You are not allowed to post in this channel");
            }
            else if (currentChat.isSecret) {
                if (currentChat.isPending) {
                    return i18n.tr("Waiting for other party to accept the secret chat...");
                } else if (currentChat.isClosed) {
                    return i18n.tr("Secret chat has been closed");
                }
            }
            return "";
        }
    }

    Rectangle {
        id: input
        anchors {
            left: parent.left
            right: parent.right
            bottom: showKeyboardLoader.top
        }
        height: entry.height + Suru.units.gu(2)
        color: Suru.backgroundColor
        visible: currentChat.isWritable
        Rectangle {
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
            }
            height: Suru.units.dp(1)
            color: Suru.neutralColor
        }

        AttachPanel {
            id: attach_panel_object
            function requestMedia(mediaType) {
                Qt.inputMethod.hide();
                mediaImporter.contentType = mediaType;
                mediaImporter.requestMedia();
            }

            function requestLocation() {
                UITK_Popups.PopupUtils.open(sendLocationConfirmationDialog)
            }
            onPhotoRequested: requestMedia(ContentHub.ContentType.Pictures)
            onDocumentRequested: requestMedia(ContentHub.ContentType.Documents)
            onVideoRequested: requestMedia(ContentHub.ContentType.Videos)
            onAudioRequested: requestMedia(ContentHub.ContentType.Music)
            onContactRequested: requestMedia(ContentHub.ContentType.Contacts)
            onLocationRequested: requestLocation()
        }

        MediaImport {
            id: mediaImporter

            onMediaReceived: {
                var fileNames = [ ]
                for ( var i = 0; i < importedFiles.length; i++ ) {
                    var filePath = String(importedFiles[i].url).replace('file://', '')
                    var fileName = filePath.split("/").pop();
                    fileNames.push(filePath)
                }
                for (var i = 0; i < fileNames.length; i++) {
                    switch(contentType) {
                        case ContentHub.ContentType.Pictures:
                            AppActions.chat.sendPhoto(fileNames[i], "");
                        break;
                        case ContentHub.ContentType.Videos:
                            AppActions.chat.sendVideo(fileNames[i], "");
                        break;
                        case ContentHub.ContentType.Music:
                            AppActions.chat.sendAudio(fileNames[i], "");
                        break;
                        case ContentHub.ContentType.Contacts:
                            AppActions.chat.sendContact(fileNames[i], "");
                        break;
                        default:
                            AppActions.chat.sendDocument(fileNames[i], "");
                    }
                }
            }
        }
        RowLayout {
            anchors.fill: parent
            anchors.margins: Suru.units.gu(1)
            spacing: Suru.units.gu(1)

            UITK.TextArea {
                id: entry
                activeFocusOnPress: true
                autoSize: true
                selectByMouse: true
                mouseSelectionMode: TextEdit.SelectWords
                wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
                placeholderText: i18n.tr("Type a message...")

                Layout.fillHeight: true
                Layout.fillWidth: true

                function send() {
                    Qt.inputMethod.commit();

                    if (d.chatState === ChatState.EditingMessage) {
                        switch (d.messageOfInterest.content.type) {
                            case QTdObject.MESSAGE_TEXT:
                                AppActions.chat.sendEditMessageText(d.messageOfInterest.id, text.trim())
                                break;
                            case QTdObject.MESSAGE_PHOTO:
                                AppActions.chat.sendEditMessageCaption(d.messageOfInterest.id, text.trim())
                                break;
                            default:
                                console.warn("Unsupported edit message type");
                        }
                    } else if (d.chatState === ChatState.ReplyingToMessage) {
                        AppActions.chat.sendReplyToMessage(d.messageOfInterest.id, text.trim());
                    } else {
                        AppActions.chat.sendMessage(text.trim());
                    }

                    text = "";
                    d.chatState = ChatState.Default;
                }
                function sendIfEnter(event) {
                    if (event.modifiers & Qt.ShiftModifier) {
                        send();
                        return;
                    }
                    event.accepted = false;
                }

                Keys.onEnterPressed: sendIfEnter(event)
                Keys.onReturnPressed: sendIfEnter(event)

                onLengthChanged: {
                    if (!typing_timer.running) {
                        typing_timer.start()
                        AppActions.chat.sendChatAction();
                    }
                }

                Timer {
                    id: typing_timer
                    interval: 5000
                }
            }
            UITK.StyledItem {
                height: entry.implicitHeight
                width: height
                UITK.Icon {
                    anchors.fill: parent
                    name: "attachment"
                    color: Suru.foregroundColor
                    Suru.textStyle: Suru.SecondaryText

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            attach_panel_object.visible = !attach_panel_object.visible;
                        }
                    }
                }
            }
            UITK.StyledItem {
                visible: showKeyboardLoader.active
                height: entry.implicitHeight
                width: height
                UITK.Icon {
                    anchors.fill: parent
                    name: showKeyboardLoader.item.anchors.bottomMargin < 0 ? "input-dialpad-symbolic" : "down"
                    color: Suru.foregroundColor
                    Suru.textStyle: Suru.SecondaryText
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        hoverEnabled: false
                        onClicked: {
                            if (UbuntuApplication.inputMethod.visible) {
                                UbuntuApplication.inputMethod.hide()
                                showKeyboardLoader.item.hidden = false
                            } else {
                                showKeyboardLoader.item.hidden = !showKeyboardLoader.item.hidden
                            }
                        }
                    }
                }
            }
            Image {
                visible: entry.displayText.trim() !== ""
                sourceSize.height: height
                source: "qrc:/qml/icons/send.png"
                height: entry.implicitHeight
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        entry.send();
                    }
                }
            }
        }
    }

    Component {
        id: sendLocationConfirmationDialog
        PopupDialog {
            text: i18n.tr("Do you want to share your location with %1?").arg(currentChat.title)
            confirmButtonColor: UITK.UbuntuColors.green
            confirmButtonText: i18n.tr("Send")
            onConfirmed: {
                messageListPage.locationWaitDialog = UITK_Popups.PopupUtils.open(locationWaitDialog)
                AppActions.chat.requestLocation();
            }
        }
    }

    Component {
        id: locationWaitDialog
        PopupWaitCancel {
            text: i18n.tr("Requesting location from OS...")
            onFinished: {
                AppActions.chat.sendLocation();
            }
            onCancelled: {
                AppActions.chat.cancelLocation();
            }
        }
    }

    AppListener {
        Filter {
            type: ChatKey.stopWaitLocation
            onDispatched: {
                UITK_Popups.PopupUtils.close(messageListPage.locationWaitDialog);
            }
        }
    }

    Loader {
        id: showKeyboardLoader
        active: Telegram.chats.currentChat.replyMarkupMessageId != 0 && Telegram.chats.currentChat.replyMarkupMessage.replyMarkup.type == QTdObject.REPLY_MARKUP_SHOW_KEYBOARD
        anchors.bottomMargin: active ? item.anchors.bottomMargin : 0
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        sourceComponent: ReplyMarkupShowKeyboard {
            replyMarkup: Telegram.chats.currentChat.replyMarkupMessage.replyMarkup
        }
    }

    QtObject {
        id: d

        property int chatState: ChatState.Default
        property
        var messageOfInterest: null
    }

    AppListener {
        Filter {
            type: ChatKey.requestEditMessage
            onDispatched: {
                d.chatState = ChatState.EditingMessage;
                d.messageOfInterest = message.message;
                switch (message.message.content.type) {
                    case QTdObject.MESSAGE_TEXT:
                        entry.text = message.message.content.text.text;
                        break;
                    case QTdObject.MESSAGE_PHOTO:
                        entry.text = message.message.content.caption.text;
                        break;
                }
            }
        }

        Filter {
            type: ChatKey.requestReplyToMessage
            onDispatched: {
                d.chatState = ChatState.ReplyingToMessage
                d.messageOfInterest = message.message;
            }
        }

    }

    onCurrentMessageIndexChanged: {
        msgList.currentIndex = currentMessageIndex
        msgList.positionViewAtIndex(currentMessageIndex, ListView.Center);
    }
}
